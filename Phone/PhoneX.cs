﻿using System.Globalization;

namespace Phone
{
    public class PhoneX : Phone
    {

        public override void ConnectPhone(NetworkTypes type)
        {
            if (type == NetworkTypes.EDGE)
            {
                _lte = false;
            }
            else
            {
                _lte = true;
            }
        }


        public override string Photo(string photo)
        {
            string titleCase = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(photo);
            return titleCase;
        }

    }
}
