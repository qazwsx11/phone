﻿using System;

namespace Phone
{
    class PhoneFactory
    {
        Random rnd = new Random();

        public Phone CreatePhone(DeliveryTypes devType, string address)
        {           
            var phn = new Phone();              
            
            if (devType == DeliveryTypes.Hermes)
            {
                var hermesDev = new Hermes();
                hermesDev.UniqueNumber = rnd.Next(100000, 999999);
                // hermesDev.uniqueNumber = rnd.Next(100000, 999999);
                hermesDev.Delivery(address);                
            }
            else
            {
                var hermesDroneDev = new HermesDrone();
                hermesDroneDev.UniqueNumber = rnd.Next(100000, 999999);
                hermesDroneDev.Delivery(address);                
            }

            return phn;
        }
                

        //public PhoneX CreatePhoneX(DeliveryTypes devType, string address)
        //{
        //    var phnX = new PhoneX();
            
        //    if (devType == DeliveryTypes.Hermes)
        //    {
        //        var hermesDev = new Hermes();
        //        hermesDev.uniqueNumber = rnd.Next(100000, 999999);
        //        hermesDev.Delivery(address);
        //    }
        //    else
        //    {
        //        var hermesDroneDev = new HermesDrone();
        //        hermesDroneDev.uniqueNumber = rnd.Next(100000, 999999);
        //        hermesDroneDev.Delivery(address);                
        //    }

        //    return phnX;
        //}


    }
}
