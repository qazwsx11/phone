﻿using System;
using System.Globalization;

namespace Phone
{
    public class Phone 
    {  

        protected bool _lte;

        public virtual void ConnectPhone(NetworkTypes type)
        {
            _lte = false;            
        }
        
        public void PhoneNetworkInfo()
        {
            if (_lte == false)
            {
                Console.WriteLine("EDGE");
            }
            else
            {
                Console.WriteLine("EDGE and LTE");
            }       
        }

        public virtual string Photo(string photo)
        {
            string titleCase = CultureInfo.CurrentCulture.TextInfo.ToLower(photo);
            return titleCase;            
        }

        public void Calling(string number)
        {
            if (_lte == false)
            {
                Console.WriteLine($"{number} via EDGE");
            }
            else
            {
                Console.WriteLine($"{number} via LTE");
            }
        }
    }
}
