﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Phone
{
    public abstract class DeliveryBase
    {
        private  List<DeliveryBase> DeliveryCompanies = new List<DeliveryBase>();

        public void AddToDeliveryCompaniesList(DeliveryBase company)
        {
            DeliveryCompanies.Add(company);
        }

        private int _un;

        public int UniqueNumber
        {
            get { return _un; }
            set { _un = value; }
        }

        public virtual void Delivery(string address)
        {
            Console.WriteLine($"Product with id {_un} is delivered to {address}");
        }
    }
}
